function xmlLoad(){  
    $.ajax({  
        async: false,
        url:'HiRoism_ebisu.xml', 
        type:'get',  
        dataType:'xml',  
        timeout:1000,  
        success:parse_xml  
    });  
}
function parse_xml(xml,status){  
    if(status!='success')return;
    var container = document.getElementById("tw-info");
    var htmlstr = "";

    var entry = $(xml).find('entry'); // xmlの中身のentry取得
    var day = entry.find('published'); // entryの中身の日時を取得
    console.log(day);
    var entry = entry.find('content');  // entryの中身の投稿内容を取得
    var posts = 5;
    
    
    for (var i=0; i < posts; i++){
        
        var defdate = day[i].innerHTML; 
        var date = defdate.replace(/-/g,'/');
        console.log(date);
        var pdate = new Date(date);
        var strdate = pdate.getFullYear() + '年' + (pdate.getMonth() + 1) + '月' + pdate.getDate() + '日';
        var content = entry[i].innerHTML;
        var img_normal = content;
        var tw_image = img_normal.match("<img.*jpg.*?>");
        var tw_cont = img_normal.replace(tw_image,"");
        function formatTwitterString(tw_cont){
         //   str = str.replace(/(http:\/\/vimeo.com\/)([-_\w\.]+)/gm,'<strong>$2</strong>');
            tw_cont = tw_cont.replace(/((ftp|http|https?):\/\/([-\w\.]+)+(:\d+)?(\/([-\w/_\.]*(\?\S+)?)?)?)/gm,'<a href="$1" target="_blank">$1</a>');
            tw_cont = tw_cont.replace(/@(\w+)/gm,'<a href="http://twitter.com/$1" target="_blank">@$1</a>');
            tw_cont = tw_cont.replace(/#(\w+)/gm,'<a href="http://search.twitter.com/search?q=$1" target="_blank">#$1</a>');
            tw_cont = tw_cont.replace(/<a href=\"http:\/\/vimeo.com\/([-_\w\.]+)(" target=\"_blank\">)(http:\/\/vimeo.com\/[-_\w\.]+)<\/a>/gm,'<a href="$3">$3</a><iframe class="thumb" src="http://player.vimeo.com/video/$1?title=0&amp;byline=0&amp;portrait=0" width="100" height="56" frameborder="0"></iframe>');
            return tw_cont;
         } 
        htmlstr += '<hr>'; 
        htmlstr += '<div class="tw_cont"><p>' + strdate +'&nbsp;'+ formatTwitterString(tw_cont) + '</p></div></div>';
    }
    container.innerHTML = htmlstr;
}
$(function(){  
    xmlLoad();  
}); 