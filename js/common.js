function ieInitPropaties() {
    // IEでページを読み込んだ直後に初期設定などがうまく反映されない時にここで設定する
    // fixedMenu.topFixedDisplayViewing_resetOffset();
}

/*********************************************************************************
 loading
**********************************************************************************/

$('head').append(
    '<style type="text/css">#container { display: none; } #fade, #loader { display: block; }</style>'
);
 
jQuery.event.add(window,"load",function() { // 全ての読み込み完了後に呼ばれる関数
    var pageH = $("#container").height();
    var w = $(window).width() * 1.05; //IEに対応させる為1.05倍
    $("#fade").css("height", pageH).delay(900).fadeOut(800);
    $("#loader").delay(600).fadeOut(300);
    $("#container").css("display", "block");

    $('#container').attr('width', w);
});

$('body').scrollspy({ target: '#site_header', offset:'0' })

/*********************************************************************************
 Utility
**********************************************************************************/
// ブラウザーに関する情報
var brwsInfo = {
    f_win: false,
    f_ie: false,
    f_ff: false,
    f_chrome: false,
    f_ipad: false,
    f_iphone:false,
    f_ios: false,
    f_android: false,
    f_mobile: false,
    f_tablet: false,
    brws_ver: false,
    os_ver: false,
    //f_cookie: checkCookie(),
    get: null,
    construct: function() {
        var usrAg = window.navigator.userAgent.toLowerCase();
        var appVersion = window.navigator.appVersion.toLowerCase();
        var bodyObj = document.getElementsByTagName('body')[0];
        
        if (usrAg.match(/win(dows\s)?/i) !== null) {
            bodyObj.className = bodyObj.className + ' win';
            this.f_win = true;
        }
        if (usrAg.indexOf("msie") > -1) {
            this.f_ie = true;
            bodyObj.className = bodyObj.className + ' ie';
            try {
                this.brws_ver = usrAg.match(/msie\s(\d+)\./i)[1];
                bodyObj.className = bodyObj.className + ' ie_'+this.brws_ver;
            } catch(e) {}
        } else if (usrAg.indexOf('FireFox') > -1 || usrAg.indexOf('firefox') > -1) {
            this.f_ff = true;
            bodyObj.className = bodyObj.className + ' firefox';
        } else if (usrAg.indexOf('CriOS') > -1 || usrAg.indexOf('crios') > -1 || usrAg.indexOf('Chrome') > -1 || usrAg.indexOf('chrome') > -1) {
            this.f_chrome = true;
            bodyObj.className = bodyObj.className + ' chrome';
        } else if (usrAg.indexOf('Safari') > -1 || usrAg.indexOf('safari') > -1) {
            this.f_ff = true;
            bodyObj.className = bodyObj.className + ' safari';
        }
        if (usrAg.indexOf('iPad') > -1 || usrAg.indexOf('ipad') > -1) {
            this.f_ipad = true;
            this.f_tablet = true;
            this.f_mobile = true;
            this.f_ios = true;
            bodyObj.className = bodyObj.className + ' tablet mobile ipad';
        } else if (usrAg.indexOf('iPhone') > -1 || usrAg.indexOf('iphone') > -1) {
            this.f_iphone = true;
            this.f_mobile = true;
            this.f_ios = true;
            bodyObj.className = bodyObj.className + ' smart mobile iphone';
        } else if (usrAg.indexOf('Android') > -1 || usrAg.indexOf('android') > -1) {
            this.f_tablet = true;
            this.f_mobile = true;
            this.f_android = true;
            bodyObj.className = bodyObj.className + ' android';
            try {
                this.os_ver = usrAg.match(/android\s(\d+\.\d+)/i)[1];
                bodyObj.className = bodyObj.className + ' android_'+parseInt(this.os_ver);
            } catch(e) {}
        }
        
        // GET パラメータを取得する
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        if(hashes != location.href) {
            for(var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            this.get = vars;
        }
    }
};
// IE 対策
if(brwsInfo.f_ie) {
    if (!('console' in window)) {
        window.console = {};
        window.console.log = function(str){
            return str;
        };
    }
}


// ウィンドウが body より大きい時の調整
var htmlBodyHeightAdjuster = {
    f_adjust: null,
    originalHeight: false,
    adjust: function() {
        if(this.f_adjust !== null && this.originalHeight !== false) {
            if(jQuery(window).height() >= this.originalHeight ) { jQuery("html").css("height","100%"); jQuery("body").css("height","100%"); jQuery("body").addClass("adjust-height"); }
            else { jQuery("html").css("height",""); jQuery("body").css("height",""); jQuery("body").removeClass("adjust-height"); }
        } else {
            this.init();
        }
    },
    init: function() {
        if(screen.height <= document.body.clientHeight) { this.f_adjust = false; }
        else {
            this.f_adjust = true;
            this.originalHeight = document.body.clientHeight;
            this.adjust();
        }
        return this.f_adjust;
    }
};



jQuery(window).on('load', function(){
    brwsInfo.construct();
    
    if(!brwsInfo.f_ie || brwsInfo.brws_ver >= 10) {
        //jQuery('img.svg').attr('src', jQuery('img.svg').attr('src').replace('.png','.svg')); // svg を png に変換する
    }
    // .full_bg エリア内の画像を調整
    areaFullsizeImg(jQuery('body .full_bg .carousel-inner .item'));
    
    // 画像のロールオーバー
    jQuery(function(){
        jQuery('a img').hover(function(){
            jQuery(this).attr('src', jQuery(this).attr('src').replace('_off', '_on'));
        }, function(){
            if (!jQuery(this).hasClass('current')) {
                jQuery(this).attr('src', jQuery(this).attr('src').replace('_on', '_off'));
            }
        });
    });
    
    // ウィンドウが body より大きい時の調整
    if(htmlBodyHeightAdjuster.init()) {
        var timer = false;
        jQuery(window).resize(function() {
            if (timer !== false) { clearTimeout(timer); }
            timer = setTimeout(function() { htmlBodyHeightAdjuster.adjust(); }, 100);
        });
    }
    
    //if(f_iphone || f_ipad || f_android) { cnvTelLink(); }
    
    if(brwsInfo.f_ie) {
        setTimeout(function() {
                ieInitPropaties();
        }, 2);
    }
});



jQuery(window).resize(function(){
    areaFullsizeImg(jQuery('.full_bg .carousel-inner .item'));
});



// エリア内の画像をエリアに合うように拡大縮小する

function areaFullsizeImg(el, callbackFunc) {
    for(var i=0; i < el.length; i++) {
        var areaOb = el[i];
        var cOb = jQuery(areaOb).find('img:first');
        if(cOb.length > 0) {
            var o = cOb.get(0);
            
            var areaW = jQuery(areaOb).width();
            var areaHeight = jQuery(areaOb).height();
            var minWidth = areaW;
            
            if(minWidth !== null && minWidth > areaW) { areaW = minWidth; }
            
            jQuery(o).css({'height': 'auto', 'width': 'auto', 'position': 'relative'});
            var wWidth = o.width;
            var wHeight = o.height;
            var heightRate = wHeight / areaHeight;
            var widthRate = wWidth / areaW;
            //console.log('wWidth : ',wWidth);
            //console.log('areaW : ',areaW);
            //console.log('wHeight : ',wHeight);
            //console.log('areaHeight : ',areaHeight);
            
            if(heightRate > 1) {
                jQuery(o).height(areaHeight);
                wHeight = areaHeight;
                wWidth /= heightRate;
                widthRate = wWidth / areaW;
                heightRate = 1;
            }
            if(wHeight < areaHeight) {
                wHeight = areaHeight;
                wWidth /= heightRate;
                widthRate = wWidth / areaW;
                jQuery(o).height(areaHeight);
                jQuery(o).css('width', 'auto');
                heightRate = 1;
            }
            if(wWidth < areaW) {
                wWidth = areaW;
                wHeight /= widthRate;
                heightRate = wWidth / areaHeight;
                widthRate = 1;
                jQuery(o).width(areaW);
                jQuery(o).css('height', 'auto');
            }
            var topPos = (areaHeight-wHeight)/2;
            var leftPos = (areaW-wWidth)/2;
            jQuery(o).css({'top': topPos+'px', 'left': leftPos+'px'});
            //console.log('-wWidth : ',wWidth);
            //console.log('-areaW : ',areaW);
            //console.log('-wHeight : ',wHeight);
            //console.log('-areaHeight : ',areaHeight);
        }
    }
    if(callbackFunc !== undefined) { callbackFunc(); }
}
// エリア内の .full_bg .carousel-inner .item 内の画像をエリアに合うように拡大縮小する
function areaFullsizeBgImg(o, callbackFunc) {
    areaFullsizeImg(jQuery(o).find('.full_bg .carousel-inner .item'), callbackFunc);
}

// 後処理用のエリア内画像拡大処理
function areaFullsizeImgAfter(o) {
    if(!jQuery(o).hasClass('image_full_size')) {
        areaFullsizeImg(jQuery(o).find('.full_bg'));
        jQuery(o).addClass('image_full_size');
    }
}

/*********************************************************************************
 Fixed menu
 version: 0.1
 Create date: 2012/10/24
 Creater: Shigeo Tsukamoto
 
 jQuery を必要とします。
**********************************************************************************/
var fixedMenu = {
    // ver 0.1
    f_moveFixedDisplay: true,
    f_active: false,
    fixedType: 'top',
    createMenuPos: 'nav',
    topFixedDispPos: 'to_top_menu_area',
    topFixedEle: null,
    topFixedDisp: null,
    topFixedDispOffset: null,
    fixedFlowOpacity: 0.6,
    menuHtml: '',
    triggerPos: false,
    
    
    construct: function() {
        if(
                jQuery("#"+this.topFixedDispPos).length > 0
        ) {
            this.f_active = true;
            // Create menu
            if(this.menuHtml != '') { jQuery('#'+this.createMenuPos).after(this.menuHtml); }
            
            this.topFixedEle = jQuery("#"+this.topFixedDispPos);
            jQuery(this.topFixedEle).html('<div class="fixed_area">'+jQuery(this.topFixedEle).html()+'</div>');
            this.topFixedDisp = jQuery("#"+this.topFixedDispPos+' .fixed_area');
            jQuery(this.topFixedDisp).css({'position' : 'relative'});
            
            this.setTriggerPos();
            
            //this.topFixedDispOffset = this.topFixedDisp.offset();
            //this.topFixedDisplayViewing();
            this.topFixedDisp.get(0).__fxm_min_opacity = this.fixedFlowOpacity;
            this.topFixedDisp.hover(
                function(){
                    if(jQuery(this).hasClass('fixed_view')) {
                        jQuery(this).fadeTo("fast",1);
                        return false;
                    } else if(jQuery(this).css("opacity") != 1) {
                        jQuery(this).css({opacity:1});
                    }
                },
                function(){
                    if(jQuery(this).hasClass('fixed_view')) {
                        jQuery(this).fadeTo("fast",this.__fxm_min_opacity);
                        return false;
                    }
                }
            );
            // document の高さが変わった時にリセットする
            
            this.topFixedDisplayViewing_resetOffset();
            if(!brwsInfo.f_ipad && !brwsInfo.f_iphone) {
            }
        } else {
            this.deconstruct();
        }
    },
    
    deconstruct: function () {
        this.f_active = false;
        this.topFixedDisp = null;
        this.topFixedDispOffset = null;
    },
    
    getActive: function () {
        return this.f_active;
    },
    
    setTriggerPos: function() {
        this.topFixedDispOffset = this.topFixedEle.offset();
        if(this.triggerPos == 'bottom') {
            this.topFixedDispOffset.top += jQuery(this.topFixedEle).height();
        }
    },
    setActiveTopPostion: function (pos) {
        if(this.topFixedDispOffset === null) { this.topFixedDisp.offset(); }
        this.topFixedDispOffset.top = pos;
    },
    setActiveLeftPostion: function (pos) {
        if(this.topFixedDispOffset === null) { this.topFixedDisp.offset(); }
        this.topFixedDispOffset.left = pos;
    },
    setMenuHtml: function (html) {
        this.menuHtml = html;
    },
    setDefaultMenuHtml: function() {
        this.menuHtml = '<div id="'+this.topFixedDispPos+'"><a href="#"><span class="button">↑Top</span></a></div>';
    },
    
    off: function () {
        if(this.topFixedDispPos !== null) {
            jQuery("#"+this.topFixedDispPos).hide();
        }
    },
    
    topFixedDisplayViewing: function () {
        //if(!f_ie && !f_ipad && !f_iphone) {
        if(true || !brwsInfo.f_ipad && !brwsInfo.f_iphone) {
            if(this.f_active && this.f_moveFixedDisplay && this.topFixedDispOffset !== null) {
                switch(this.fixedType) {
                    case 'top':

                        if(jQuery(window).scrollTop() > 392) {
                            if(!this.topFixedDisp.hasClass('fixed_view')) {
                                if(this.triggerPos === false) {
                                    this.topFixedDisp.stop(true, true).fadeTo("fast",this.fixedFlowOpacity);
                                } else {
                                    this.topFixedDisp.css({'position' : 'fixed', 'top' : '-'+jQuery(this.topFixedDisp).height()+'px'});
                                    this.topFixedDisp.stop(true, true).animate({ 'opacity' : this.fixedFlowOpacity, 'top' : 0}, 500);
                                }
                                this.topFixedDisp.addClass("fixed_view top_fixed_view");
                            }
                        } else {
                            var windowsize = $(window).width();
                            if(windowsize < 480){
                                jQuery('.overlayer').css({'position' : 'relative', 'top': '440px', 'z-index' : 1});
                            }else{
                                if(jQuery(window).scrollTop() > 110){
                                    var PosTop = 790 - jQuery(window).scrollTop();
                                    jQuery(this.topFixedDisp).css({'position' : 'relative', 'top': PosTop+'px', 'z-index' : 3});
                                    jQuery('.overlayer').css({'position' : 'relative', 'top': PosTop - 0 +'px', 'z-index' : 1});
                                }else{
                                    jQuery(this.topFixedDisp).css({'position' : 'fixed', 'top': '583px'});
                                    jQuery('.overlayer').css({'position' : 'relative', 'top': '779px', 'z-index' : 1});
                                    
                                }
                            }
                            
                            
                            if(this.topFixedDisp.hasClass('fixed_view')) {
                                this.topFixedDisp.css({opacity:1});
                                this.topFixedDisp.removeClass("fixed_view top_fixed_view");
                            } else if(this.topFixedDisp.css("opacity") != 1) {
                                this.topFixedDisp.css({opacity:1});
                            }
                        }
                        break;
                    case 'bottom':
                        var winBottom = jQuery(window).scrollTop() + jQuery(window).height();
                        var itemBottom = this.topFixedDispOffset.top + this.topFixedDisp.height();
                        if(winBottom > itemBottom) {
                            this.topFixedDisp.stop(true, true);
                            if(this.topFixedDisp.hasClass('fixed_view')) {
                                this.topFixedDisp.css({opacity:1});
                                this.topFixedDisp.removeClass("fixed_view top_fixed_view");
                            } else if(this.topFixedDisp.css("opacity") != 1) {
                                this.topFixedDisp.css({opacity:1});
                            }
                        } else {
                            if(!this.topFixedDisp.hasClass('fixed_view')) {
                                this.topFixedDisp.stop(true, true).fadeTo("fast",this.fixedFlowOpacity);
                                this.topFixedDisp.addClass("fixed_view top_fixed_view");
                            }
                        }
                        break;
                }
            }
        }
    },
    topFixedDisplayViewing_resetOffset: function () {
        if(this.f_active) {
            this.topFixedDisp.removeClass("top_fixed_view");
            //this.topFixedDispOffset = this.topFixedDisp.offset();
            this.setTriggerPos();
            this.topFixedDisplayViewing();
        }
    }
};

jQuery(window).on('load', function(){
    fixedMenu.triggerPos  = 'bottom';
    fixedMenu.topFixedDispPos = 'header_menu';
    fixedMenu.fixedFlowOpacity = 1;
});


if(fixedMenu === undefined || eval(fixedMenu) !== false) {
    jQuery(document).change( function() {
        if(fixedMenu.getActive()) {
            fixedMenu.topFixedDisplayViewing_resetOffset();
            if(brwsInfo.f_ie) {
                setTimeout(function(){
                    fixedMenu.topFixedDisplayViewing_resetOffset();
                },3000); // IE は遅延させないとリセットがうまく動作しない
            }
        }
    });
    
    jQuery(window).on('load', function(){
        fixedMenu.construct();
    });
    jQuery(window).scroll(function () {
        if(fixedMenu.getActive()) {
            fixedMenu.topFixedDisplayViewing();
        }
    });
    jQuery(window).resize(function () {
        if(fixedMenu.getActive()) {
            fixedMenu.setTriggerPos();
            // fixedMenu.topFixedDisplayViewing_resetOffset();
        }
    });
} else {
    jQuery(window).on('load', function(){
        fixedMenu.off();
    });
}

/*********************************************************************************
 Smooth anchor scroll
 version: 0.1.5
 Create date: 2014/9/19
 Creater: Shigeo Tsukamoto
 
 jQuery を必要とします。
 動作をつけたいアンカーに scrollObj の className で指定したクラスをつけると
 アンカーへのスクロール移動がスムーズに行くようになります。
**********************************************************************************/

var scrollObj = {
    className: 'scroll_to_anchor',
    interval: 50,
    safeMovePixel:30,
    
    f_posCheck:false,
    ancObj:null,
    jump:null,
    toTop:null,
    toLeft:null,
    befTop:null,
    befLeft:null,
    scrollInterval:null,
    toObj:null,
    
    startScroll: function (o, f_toSet) {
        this.ancObj = o;
        var hObj;
        if(f_toSet !== true) {
            this.jump = o.hash;
        } else {
            this.jump = this.toObj;
        }
        hObj = jQuery(this.jump).get();
        var pos;
        if(hObj.length != 0) {
            pos = jQuery(this.jump).offset();
            var winTopMax = jQuery(document).height() - jQuery(window).height();
            if(winTopMax < pos.top) { this.toTop = winTopMax; }
            else { this.toTop = pos.top; }
            var winLeftMax = jQuery(document).width() - jQuery(window).width();
            if(winLeftMax < pos.top) { this.toLeft = winLeftMax; }
            else { this.toLeft = pos.left; }
        } else {
            this.toTop = 0;
            this.toLeft = 0;
        }
        this.ancObj.removeAttribute('href');
        this.scrollToPosition();
        
        return false;
    },
    
    scrollToPosition: function() {
        var nowX = jQuery(window).scrollLeft();
        var nowY = jQuery(window).scrollTop();
        if(nowX != 0 || nowY != 0) {
        // 画面をプログラム以外で動かしているかをチェック
            if(this.f_posCheck) {
                if(
                        this.befTop !== null && this.befLeft !== null && (
                            this.befTop - nowY > this.safeMovePixel || nowY - this.befTop > this.safeMovePixel ||
                            this.befLeft - nowX > this.safeMovePixel || nowX - this.befLeft > this.safeMovePixel
                        )
                ) {
                    scrollObj.scrollCancel();
                    return false;
                }
            }
            
            if((nowX - this.toLeft) != 0 || (nowY - this.toTop) != 0){
                var moveX;
                var moveY;
                if((nowX - this.toLeft) < 3 && (nowX - this.toLeft) > -3) { moveX = this.toLeft; }
                else { moveX = nowX+((this.toLeft - nowX)/3); }
                if((nowY - this.toTop) < 3 && (nowY - this.toTop) > -3) { moveY = this.toTop; }
                else { moveY = nowY+((this.toTop - nowY)/3); }
                
                this.scrollAction(moveX, moveY);
                
                this.scrollInterval = setTimeout(function() { scrollObj.scrollToPosition(); }, this.interval);
                return false;
            } else {
                clearTimeout(this.scrollInterval);
                this.resetScroll();
            }
        } else {
            clearTimeout(this.scrollInterval);
            this.resetScroll();
        }
        return false;
    },
    
    scrollAction: function(x, y) {
        window.scrollTo(x, y);
        var nowX = jQuery(window).scrollLeft();
        var nowY = jQuery(window).scrollTop();
        if(nowX == this.befLeft) { this.toLeft = nowX; }
        if(nowY == this.befTop) { this.toTop = nowY; }
        this.befLeft = nowX;
        this.befTop = nowY;
    },
    
    scrollCancel: function () {
        if(this.scrollInterval !== null) { clearTimeout(this.scrollInterval); }
        this.resetScroll();
    },
    
    resetScroll: function() {
        if(this.jump != '' &&this.jump !== null && this.ancObj !== null) {
            if(this.toObj === null) {
                this.ancObj.setAttribute('href', this.jump);
                //this.ancObj.setAttribute('class', 'active scroll_to_anchor');
                //console.log(this.ancObj);
            } else {
                this.toObj = null;
            }
        }

        //if(jQuery(this.ancObj).parents('#header_menu').length > 0){
            // scrool spy
            if(this.ancObj !== null || this.ancObj != undefined) {
                jQuery(this.ancObj).parent().addClass('active');

            }
       // }

        this.jump = null;
        this.ancObj = null;
        this.toTop = null;
        this.toLeft = null;
        this.befLeft = null;
        this.befTop = null;
        this.scrollInterval = null;

    }
};
jQuery(window).on('load', function() {
    var ancList = jQuery("a."+scrollObj.className).get();
    
    //キー入力などがあった場合にスクロールを止める処理
    jQuery(window).keydown(function(e){ scrollObj.scrollCancel(); });
    jQuery(window).mousedown(function(){ scrollObj.scrollCancel(); });
    try{
        jQuery(window).mousewheel(function(){ scrollObj.scrollCancel(); });
    } catch(e) {
        scrollObj.f_posCheck = true;
    }
    
    for(i = 0, max = ancList.length; i < max; i++) {
        if(ancList[i].hash !== undefined && ancList[i].hidden !== true) {
            if (ancList[i].addEventListener) {
              ancList[i].addEventListener("click", scrollToAnchorAction, false);
            } else if (ancList[i].attachEvent) {
              ancList[i].attachEvent("click", scrollToAnchorAction);
            } else  {
              ancList[i].click = scrollToAnchorAction;
            }
        }
    }
});

function scrollToAnchorAction() {
    scrollObj.startScroll(this);
}

function scrollToPageTopAction(o) {
    scrollObj.toObj = jQuery('body');
    scrollObj.startScroll(o, true);
}
// END : Smooth anchor scroll

// リンクの onclick に仕込んで、クラスに disabled が設定されている時にはリンクで移動させないようにする。
function linkJump(o) {
    if(jQuery(o).hasClass('disabled')) { return false; }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
// bx slider
// 
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////
jQuery(document).ready(function(){
 $('.bxslider').bxSlider({
     auto: true,
     pager: false,
     controls: false,
     minSlides: 1,
     maxSlides: 4,
     moveSlides:1,
     autoReload:true,
     preloadImages: 'all',
     adaptiveHeight: true,
     speed: 5000,
     pause: 0,
     easing:'linear',
     breaks: [{screen:0, slides:1, pager:false},{screen:414, slides:2},{screen:768, slides:3},{screen:1920, slides:4}],
     slideMargin: 0
   });

});

jQuery(window).on('load',function(){
   jQuery(window).resize();
});

$(function(){
    // Trigger maximage
    jQuery('#maximage').maximage({
        cycleOptions: {
            fx: 'fade',
            speed: 1500,
            timeout: 1000,
            activePagerClass: 'active',
        },
        backgroundSize: 'cover',
        verticalCenter:true,
        horizontalCenter:true,
        onFirstImageLoaded: function(){
            jQuery('#cycle-loader').hide();
            jQuery('#maximage').fadeIn('fast');
        }
    });
});

